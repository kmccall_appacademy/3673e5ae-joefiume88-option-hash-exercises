# Options Hashes
#
# Write a method `transmogrify` that takes a `String`. This method should
# take optional parameters `:times`, `:upcase`, and `:reverse`. Hard-code
# reasonable defaults in a `defaults` hash defined in the `transmogrify`
# method. Use `Hash#merge` to combine the defaults with any optional
# parameters passed by the user. Do not modify the incoming options
# hash. For example:
#
# ```ruby
# transmogrify("Hello")                                    #=> "Hello"
# transmogrify("Hello", :times => 3)                       #=> "HelloHelloHello"
# transmogrify("Hello", :upcase => true)                   #=> "HELLO"
# transmogrify("Hello", :upcase => true, :reverse => true) #=> "OLLEH"
#
# options = {}
# transmogrify("hello", options)
# # options shouldn't change.
# ```
def transmogrify(string, options={})
  defaults = {
    times: 1,
    upcase: false,
    reverse: false,
    downcase: false,
    capitalize: false,
    swapcase: false,
  }

  user_options = defaults.merge(options)
  res = string

  res = res.upcase if user_options[:upcase]
  res = res.reverse if user_options[:reverse]
  res = res.downcase if user_options[:downcase]
  res = res.swapcase if user_options[:swapcase]
  res = res.capitalize if user_options[:capitalize]

  res * user_options[:times]
end
